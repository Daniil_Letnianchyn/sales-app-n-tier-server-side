﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.N_Tier.Common.DataTransferObjects
{
    public class Employee
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
