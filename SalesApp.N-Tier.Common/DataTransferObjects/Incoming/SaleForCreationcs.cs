﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.N_Tier.Common.DataTransferObjects.Incoming
{
    public class SaleForCreation
    {
        public string CustomerId { get; set; }
        public string EmployeeId { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}