﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.N_Tier.Common.Interfaces
{
    public interface ISaleService
    {
        Task<IEnumerable<Common.DataTransferObjects.Sale>> GetAllSalesAsync();
        Task<Common.DataTransferObjects.Sale> GetSaleByIdAsync(string saleId);
        Task<Common.DataTransferObjects.Sale> CreateSaleAsync(Common.DataTransferObjects.Incoming.SaleForCreation saleForCreation);
    }
}
