﻿using SalesApp.N_Tier.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.N_Tier.Common.Interfaces
{
    public interface ICustomerService
    {
        Task<IEnumerable<DataTransferObjects.Customer>> GetAllCustomersAsync();
    }
}
