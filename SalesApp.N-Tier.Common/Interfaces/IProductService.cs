﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.N_Tier.Common.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<DataTransferObjects.Product>> GetAllProductsAsync();
    }
}
