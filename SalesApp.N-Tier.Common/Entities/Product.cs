﻿using SalesApp.N_Tier.Common.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SalesApp.N_Tier.Common.Entities
{
    public class Product : IModificationHistory
    {
        [Key]
        public Guid ProductId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        public DateTimeOffset DateModified { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
}
