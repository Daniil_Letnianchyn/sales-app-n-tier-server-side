﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.N_Tier.Common.Entities.Interfaces
{
    public interface IModificationHistory
    {
        DateTimeOffset DateModified { get; set; }
        DateTimeOffset DateCreated { get; set; }
    }
}
