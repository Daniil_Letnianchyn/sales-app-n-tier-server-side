﻿using SalesApp.N_Tier.Common.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SalesApp.N_Tier.Common.Entities
{
    public class Employee : IModificationHistory
    {
        [Key]
        public Guid EmployeeId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public DateTimeOffset DateModified { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
}
