﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SalesApp.N_Tier.DataAccess.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustomerId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerId);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductId);
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                columns: table => new
                {
                    SaleId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    CustomerId = table.Column<Guid>(nullable: false),
                    EmployeeId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.SaleId);
                    table.ForeignKey(
                        name: "FK_Sales_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sales_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sales_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerId", "DateCreated", "DateModified", "Name" },
                values: new object[,]
                {
                    { new Guid("d28888e9-2ba9-473a-a40f-e38cb54f9b35"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 88, DateTimeKind.Unspecified).AddTicks(6611), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 90, DateTimeKind.Unspecified).AddTicks(8702), new TimeSpan(0, 2, 0, 0, 0)), "'The Dude' Lebowski" },
                    { new Guid("da2fd609-d754-4feb-8acd-c4f9ff13ba96"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 90, DateTimeKind.Unspecified).AddTicks(9220), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 90, DateTimeKind.Unspecified).AddTicks(9246), new TimeSpan(0, 2, 0, 0, 0)), "'The Big' Lebowski" },
                    { new Guid("2902b665-1190-4c70-9915-b9c2d7680450"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 90, DateTimeKind.Unspecified).AddTicks(9259), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 90, DateTimeKind.Unspecified).AddTicks(9263), new TimeSpan(0, 2, 0, 0, 0)), "Walter Sobchak" },
                    { new Guid("102b566b-ba1f-404c-b2df-e2cde39ade09"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 90, DateTimeKind.Unspecified).AddTicks(9268), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 90, DateTimeKind.Unspecified).AddTicks(9272), new TimeSpan(0, 2, 0, 0, 0)), "Theodore Donald 'Donny' Kerabatsos" }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeId", "DateCreated", "DateModified", "Name" },
                values: new object[,]
                {
                    { new Guid("5b1c2b4d-48c7-402a-80c3-cc796ad49c6b"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(421), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(995), new TimeSpan(0, 2, 0, 0, 0)), "Caitlin Cleric" },
                    { new Guid("d8663e5e-7494-4f81-8739-6e0de1bea7ee"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(1422), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(1453), new TimeSpan(0, 2, 0, 0, 0)), "Solmyr Wizard" },
                    { new Guid("d173e20d-159e-4127-9ce9-b0ac2564ad97"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(1465), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(1470), new TimeSpan(0, 2, 0, 0, 0)), "Sandro Necromancer" },
                    { new Guid("40ff5488-fdab-45b5-bc3a-14302d59869a"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(1475), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(1479), new TimeSpan(0, 2, 0, 0, 0)), "Pasis Planeswalker" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "ProductId", "DateCreated", "DateModified", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("a64549d1-8879-4c1f-ac89-e123fe633ad6"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(3208), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(3587), new TimeSpan(0, 2, 0, 0, 0)), "Spaghetti", 0.7m },
                    { new Guid("be939226-9038-4f43-950f-2e860b34d377"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(4032), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(4056), new TimeSpan(0, 2, 0, 0, 0)), "Lasagna", 1.63m },
                    { new Guid("42138ae2-3391-4a72-a7a0-607ac6da7b30"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(4069), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(4073), new TimeSpan(0, 2, 0, 0, 0)), "Ravioli", 6.01m }
                });

            migrationBuilder.InsertData(
                table: "Sales",
                columns: new[] { "SaleId", "CustomerId", "DateCreated", "DateModified", "EmployeeId", "ProductId", "Quantity", "TotalPrice" },
                values: new object[] { new Guid("48e9a975-dc47-40ae-85e2-ed2dec327b97"), new Guid("d28888e9-2ba9-473a-a40f-e38cb54f9b35"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(6824), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(7196), new TimeSpan(0, 2, 0, 0, 0)), new Guid("5b1c2b4d-48c7-402a-80c3-cc796ad49c6b"), new Guid("be939226-9038-4f43-950f-2e860b34d377"), 2, 3.26m });

            migrationBuilder.InsertData(
                table: "Sales",
                columns: new[] { "SaleId", "CustomerId", "DateCreated", "DateModified", "EmployeeId", "ProductId", "Quantity", "TotalPrice" },
                values: new object[] { new Guid("d4fdba84-1ff8-4fa2-9930-68e447623c8d"), new Guid("2902b665-1190-4c70-9915-b9c2d7680450"), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(7623), new TimeSpan(0, 2, 0, 0, 0)), new DateTimeOffset(new DateTime(2020, 3, 15, 21, 22, 3, 92, DateTimeKind.Unspecified).AddTicks(7644), new TimeSpan(0, 2, 0, 0, 0)), new Guid("d173e20d-159e-4127-9ce9-b0ac2564ad97"), new Guid("42138ae2-3391-4a72-a7a0-607ac6da7b30"), 12, 72.12m });

            migrationBuilder.CreateIndex(
                name: "IX_Sales_CustomerId",
                table: "Sales",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_EmployeeId",
                table: "Sales",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_ProductId",
                table: "Sales",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sales");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
