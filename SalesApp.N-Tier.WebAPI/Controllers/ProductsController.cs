﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.N_Tier.Common.Interfaces;

namespace SalesApp.N_Tier.WebAPI.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IProductService _productService;

        public ProductsController(IProductService productService)
        {
            this._productService = productService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetProductsAsync()
        {
            IEnumerable<Common.DataTransferObjects.Product> products = await _productService.GetAllProductsAsync();
            return Ok(products);
        }
    }
}