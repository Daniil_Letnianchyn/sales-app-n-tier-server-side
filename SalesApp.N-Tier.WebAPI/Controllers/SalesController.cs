﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.N_Tier.Common.Interfaces;

namespace SalesApp.N_Tier.WebAPI.Controllers
{
    [Route("api/sales")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private ISaleService _saleService;

        public SalesController(ISaleService saleService)
        {
            this._saleService = saleService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetSalesAsync()
        {
            IEnumerable<Common.DataTransferObjects.Sale> sales = await _saleService.GetAllSalesAsync();
            return Ok(sales);
        }


        [HttpGet("{saleId}")]
        public async Task<IActionResult> GetSaleByIdAsync(string saleId)
        {
            Common.DataTransferObjects.Sale sale = await _saleService.GetSaleByIdAsync(saleId);
                
            if (sale == null)
            {
                return NotFound();
            }

            return Ok(sale);    
        }

        [HttpPost()]
        public async Task<IActionResult> AddSaleAsync([FromBody] Common.DataTransferObjects.Incoming.SaleForCreation saleForCreation)
        {
            if (saleForCreation == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Common.DataTransferObjects.Sale saleToReturn = await _saleService.CreateSaleAsync(saleForCreation);

            return Ok(saleToReturn);
        }
    }
}