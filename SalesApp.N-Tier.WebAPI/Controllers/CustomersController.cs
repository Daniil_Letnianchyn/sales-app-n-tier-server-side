﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.N_Tier.Common.Interfaces;

namespace SalesApp.N_Tier.WebAPI.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            this._customerService = customerService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetCustomersAsync()
        {
            IEnumerable<Common.DataTransferObjects.Customer> customers = await _customerService.GetAllCustomersAsync();
            return Ok(customers);
        }
    }
}