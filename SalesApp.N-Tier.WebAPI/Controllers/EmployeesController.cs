﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.N_Tier.Common.Interfaces;

namespace SalesApp.N_Tier.WebAPI.Controllers
{
    [ApiController]
    [Route("api/employees")]
    public class EmployeesController : ControllerBase
    {
        private IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            this._employeeService = employeeService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetEmployeesAsync()
        {
            IEnumerable<Common.DataTransferObjects.Employee> employees = await _employeeService.GetAllEmployeesAsync();
            return Ok(employees);
        }
    }
}