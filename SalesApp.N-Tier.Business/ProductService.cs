﻿using Microsoft.EntityFrameworkCore;
using SalesApp.N_Tier.Common.DataTransferObjects;
using SalesApp.N_Tier.Common.Interfaces;
using SalesApp.N_Tier.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.N_Tier.Business
{
    public class ProductService : IProductService
    {
        private DatabaseContext _dbContext;
        public ProductService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Common.DataTransferObjects.Product>> GetAllProductsAsync()
        {
            return await _dbContext.Products
                .Select(p => new Common.DataTransferObjects.Product
                {
                    Id = p.ProductId.ToString(),
                    Name = p.Name,
                    Price = p.Price
                })
                .ToListAsync();
        }
    }
}
