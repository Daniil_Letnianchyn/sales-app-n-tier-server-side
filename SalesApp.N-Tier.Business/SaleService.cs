﻿using Microsoft.EntityFrameworkCore;
using SalesApp.N_Tier.Common.Interfaces;
using SalesApp.N_Tier.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.N_Tier.Business
{
    public class SaleService : ISaleService
    {
        private DatabaseContext _dbContext;
        public SaleService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Common.DataTransferObjects.Sale>> GetAllSalesAsync()
        {
            return await _dbContext.Sales
                .Select(p => new Common.DataTransferObjects.Sale
                {
                    Id = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = p.Customer.Name,
                    Employee = p.Employee.Name,
                    Product = p.Product.Name,
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity
                })
                .ToListAsync();
        }

        public async Task<Common.DataTransferObjects.Sale> GetSaleByIdAsync(string saleId)
        {
            return await _dbContext.Sales
                .Where(i => i.SaleId == Guid.Parse(saleId))
                .Select(p => new Common.DataTransferObjects.Sale
                {
                    Id = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = p.Customer.Name,
                    Employee = p.Employee.Name,
                    Product = p.Product.Name,
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity
                })
                .SingleOrDefaultAsync();
        }


        public async Task<Common.DataTransferObjects.Sale> CreateSaleAsync(Common.DataTransferObjects.Incoming.SaleForCreation saleForCreation)
        {

            Common.Entities.Sale saleEntity = new Common.Entities.Sale
            {
                Customer = new Common.Entities.Customer { CustomerId = Guid.Parse(saleForCreation.CustomerId) },
                Employee = new Common.Entities.Employee { EmployeeId = Guid.Parse(saleForCreation.EmployeeId) },
                Product = new Common.Entities.Product { ProductId = Guid.Parse(saleForCreation.ProductId) },
                Quantity = saleForCreation.Quantity,
                TotalPrice = saleForCreation.Quantity * _dbContext.Products.Where(i => i.ProductId == Guid.Parse(saleForCreation.ProductId)).Select(v => v.Price).SingleOrDefault()
            };
            _dbContext.Attach(saleEntity);
            await _dbContext.SaveChangesAsync();

            return await _dbContext.Sales
                .Where(i => i.SaleId == saleEntity.SaleId)
                .Select(p => new Common.DataTransferObjects.Sale
                {
                    Id = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = p.Customer.Name,
                    Employee = p.Employee.Name,
                    Product = p.Product.Name,
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity
                })
                .SingleOrDefaultAsync();
        }
    }
}
