﻿using Microsoft.EntityFrameworkCore;
using SalesApp.N_Tier.Common.Interfaces;
using SalesApp.N_Tier.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.N_Tier.Business
{
    public class EmployeeService : IEmployeeService
    {
        private DatabaseContext _dbContext;
        public EmployeeService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Common.DataTransferObjects.Employee>> GetAllEmployeesAsync()
        {
            return await _dbContext.Employees
                .Select(p => new Common.DataTransferObjects.Employee
                {
                    Id = p.EmployeeId.ToString(),
                    Name = p.Name
                })
                .ToListAsync();
        }
    }
}
