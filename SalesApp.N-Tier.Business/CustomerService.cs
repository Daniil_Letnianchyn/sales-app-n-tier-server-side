﻿using Microsoft.EntityFrameworkCore;
using SalesApp.N_Tier.Common.Interfaces;
using SalesApp.N_Tier.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.N_Tier.Business
{
    public class CustomerService : ICustomerService
    {
        private DatabaseContext _dbContext;
        public CustomerService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Common.DataTransferObjects.Customer>> GetAllCustomersAsync()
        {
            return await _dbContext.Customers
                .Select(p => new Common.DataTransferObjects.Customer
                {
                    Id = p.CustomerId.ToString(),
                    Name = p.Name
                })
                .ToListAsync();
        }
    }
}
